import Game, { Shape } from "./game.js";
import MainViewModel from "./main_view_model.js";

const rbRock = document.querySelector(`#rbRock`);
rbRock.checked = true;
const rbPaper = document.querySelector(`#rbPaper`);
const rbScissors = document.querySelector(`#rbScissors`);

const game = new Game();
const viewModel = new MainViewModel(game);

const btn = document.querySelector(`#btnPlay`);
btn.addEventListener(`click`, () => {
  let playerShape;
  if (rbRock.checked === true) {
    playerShape = Shape.ROCK;
  }
  if (rbPaper.checked === true) {
    playerShape = Shape.PAPER;
  }
  if (rbScissors.checked === true) {
    playerShape = Shape.SCISSORS;
  }

  viewModel.playRound(playerShape);

  const winnerString = viewModel.getWinnerString();
  const shapesString = viewModel.getShapesString();

  const divResult = document.querySelector(`#divResult`);
  divResult.innerHTML = winnerString + ` ` + shapesString;
});
