export const Shape = {
  ROCK: 1,
  PAPER: 2,
  SCISSORS: 3
};

export default class Game {
  constructor() {
    this.getWinnerString = function() { return this._winnerString; };
    this.getShapesString = function() { return this._shapesString; };
  }

  playRound(playerSelection) {
    const computerSelection = Math.floor(Math.random() * 3) + 1;
    if (playerSelection === Shape.ROCK && computerSelection === Shape.ROCK) {
      this._winnerString = `Tie!`;
      this._shapesString = `Both choose rock.`;
    }

    if (playerSelection === Shape.ROCK && computerSelection === Shape.PAPER) {
      this._winnerString = `Computer wins!`;
      this._shapesString = `Paper covers rock.`;
    }

    if (playerSelection === Shape.ROCK && computerSelection === Shape.SCISSORS) {
      this._winnerString = `Player wins!`;
      this._shapesString = `Rock brakes scissors.`;
    }

    if (playerSelection === Shape.PAPER && computerSelection === Shape.ROCK) {
      this._winnerString = `Player wins!`;
      this._shapesString = `Paper covers rock.`;
    }

    if (playerSelection === Shape.PAPER && computerSelection === Shape.PAPER) {
      this._winnerString = `Tie!`;
      this._shapesString = `Both choose paper.`;
    }

    if (playerSelection === Shape.PAPER && computerSelection === Shape.SCISSORS) {
      this._winnerString = `Computer wins!`;
      this._shapesString = `Scissors cut paper.`;
    }

    if (playerSelection === Shape.SCISSORS && computerSelection === Shape.ROCK) {
      this._winnerString = `Computer wins!`;
      this._shapesString = `Rock brakes scissors.`;
    }

    if (playerSelection === Shape.SCISSORS && computerSelection === Shape.PAPER) {
      this._winnerString = `Player wins!`;
      this._shapesString = `Cissors cuts paper.`;
    }

    if (playerSelection === Shape.SCISSORS && computerSelection === Shape.SCISSORS) {
      this._winnerString = `Tie!`;
      this._shapesString = `Both choos scissors.`;
    }
  }
}
