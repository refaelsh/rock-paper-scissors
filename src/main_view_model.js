export default class MainViewModel {
  constructor(game) {
    this._game = game;
  }

  playRound(playerSelection) {
    this._game.playRound(playerSelection);
  }

  getWinnerString() {
    return this._game.getWinnerString();
  }

  getShapesString() {
    return this._game.getShapesString();
  }
}
